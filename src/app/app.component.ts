import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { EventService } from 'src/app/services/event.service'
import { OfficeService } from './services/office.service';
import { ClassroomService } from './services/classroom.service';
import { ExamService } from './services/exam.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DialogData, NewEventDialogComponent } from './components/new-event-dialog/new-event-dialog.component';
import { ExamDialogData, RegionSettingsComponent } from './components/region-settings/region-settings.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild(RouterOutlet, { static: true }) outlet;
  private eventPromise: any
  private officePromise: any
  private classroomPromise: any
  private examPromise: any
  private resources: Array<any> = []
  private sEvents: any
  private sOffices: any
  private sExams: any
  private sClassrooms: any
  private rEvents: Array<any> = []
  private requests: Array<any> = []
  title = 'apc_ustudy';
  user: object;
  isAdmin: boolean;
  constructor(private tokenService: TokenStorageService, private router: Router, private eventService: EventService, private officeService: OfficeService, private classroomService: ClassroomService, private examService: ExamService, private dialog: MatDialog) {
    this.eventPromise = this.eventService.getEvents()
    this.officePromise = this.officeService.getOffices()
    this.classroomPromise = this.classroomService.getClassrooms()
    this.examPromise = this.examService.getExams()
  }

  async ngOnInit() {
    if (this.tokenService.isLoggedIn()) {
      this.user = this.tokenService.getUser()
      if (this.tokenService.getUser().role == "Administrator") {
        this.isAdmin = true;
      }
    }
  }

  parseEvents(): void {
    this.sEvents.forEach(element => {
      var event = {
        id: element.id,
        title: element.officeName + " / " + element.actions[0].examName,
        start: new Date(element.actions.find(action => action.type == 0).date).getTime(),
        end: new Date(element.actions.find(action => action.type == 1).date).getTime(),
        overlap: element.actions.find(action => action.type == 0).isMixable,
        editable: true,
        backgroundColor: "#3788d8",
        resourceId: element.classroomId,
        extendedProps: {
          people: element.actions.find(action => action.type == 0).people,
          examId: element.actions.find(action => action.type == 0).examId,
          classroomId: element.classroomId,
          senderId: element.actions.find(action => action.type == 0).senderId
        }
      }
      if (element.status == 0) {
        event.backgroundColor = "#ffbf00"
      }
      else if (element.status == 1) {
        event.backgroundColor = "#00FF00"
        event.editable = false
      }
      else if (element.status == 2) {
        event.backgroundColor = "#FF3333"
      }
      this.rEvents.push(event)
    });
  }

  async openDialog() {
    this.sExams = await this.examPromise

    var data = new DialogData()
    data.classrooms = this.sClassrooms;
    data.exams = this.sExams;
    data.offices = this.sOffices;
    const newEventDialogConfig = new MatDialogConfig();

    newEventDialogConfig.disableClose = true;
    newEventDialogConfig.autoFocus = true;


    newEventDialogConfig.data = data;
    this.dialog.open(NewEventDialogComponent, newEventDialogConfig)
  }

  async openExamDialog() {
    this.sExams = await this.examPromise

    var data = new ExamDialogData()
    data.exams = this.sExams
    data.offices = this.sOffices

    const newExamDialogConfig = new MatDialogConfig();

    newExamDialogConfig.disableClose = true;
    newExamDialogConfig.autoFocus = true;

    newExamDialogConfig.data = data;

    this.dialog.open(RegionSettingsComponent, newExamDialogConfig)
  }



  parseResourses(): void {
    this.sClassrooms.forEach(classroom => {
      this.sOffices.forEach(office => {
        if (classroom.officeId == office.id) {
          var resourse = {
            id: classroom.id,
            office: office.name,
            title: "Аудитория на " + classroom.capacity + " мест"
          }
          this.resources.push(resourse)
        }
      })
    })
  }

  async onActivate(componentReference) {
    this.sClassrooms = await this.classroomPromise
    this.sClassrooms = this.sClassrooms.page.items
    this.sEvents = await this.eventPromise
    this.sEvents = this.sEvents.page.items
    this.sOffices = await this.officePromise
    this.sOffices = this.sOffices.page.items
    this.parseEvents()
    this.parseResourses()
    var tempEvents = this.sEvents;
    tempEvents.forEach(element => {
      element.actions.forEach(action => {
        var d = new Date(action.date)
        action.date = d.getDate() + "." + (d.getMonth() + 1) + "." + d.getFullYear() + " " +
          d.getHours() + ":" + (d.getMinutes() < 10 ? '0' : '') + d.getMinutes();
      })
      if (element.actions[0].type === 0) {
        this.requests.push(element)
      }
    });

    console.log(componentReference)
    console.log(componentReference.constructor.name)
    console.log(this.requests)
    if (componentReference.constructor.name == "ScheduleComponent") {
      componentReference.calendarOptions.events = this.rEvents
      componentReference.calendarOptions.resources = this.resources
    }
    if (componentReference.constructor.name == "AdminComponent") {
      componentReference.requestSet(this.requests)
    }
  }

  logout(): void {
    this.tokenService.signOut()

  }
}
