import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { EventService } from 'src/app/services/event.service';
import { ExamService } from 'src/app/services/exam.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  public dataSource : any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['name', 'classroom', 'exam', 'date', 'people', 'decision'];
  constructor(private tokenStorageService: TokenStorageService, private router: Router, private eventService: EventService) { }

  ngOnInit(): void {
    if (this.tokenStorageService.isLoggedIn() && this.tokenStorageService.getUser().role == "Administrator") {

    } else {
      this.router.navigate(['/signin'])
    }
  }

  public requestSet(value: any) {
    this.dataSource = new MatTableDataSource<[{}]>(value);
    this.dataSource.paginator = this.paginator;
  }

  async approve(value: any){
      await this.eventService.approve(value)
  }

  async reject(value: any){
      await this.eventService.reject(value)
  }

}
