import { Component, Inject, Injectable, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import * as moment from 'moment';
import { ThemePalette } from '@angular/material/core';
import { EventService } from 'src/app/services/event.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class DialogData {
  classrooms: any;
  exams: any;
  offices: any;
}

@Component({
  selector: 'app-new-event-dialog',
  templateUrl: './new-event-dialog.component.html',
  styleUrls: ['./new-event-dialog.component.css']
})
export class NewEventDialogComponent implements OnInit {
@ViewChild('picker') picker: any;
  public date: moment.Moment;
  public disabled = false;
  public showSpinners = true;
  public showSeconds = false;
  public touchUi = false;
  public enableMeridian = false;
  public minDate: moment.Moment;
  public maxDate: moment.Moment;
  public stepHour = 1;
  public stepMinute = 1;
  public stepSecond = 1;
  public disableMinute = false;
  public hideTime = false;

  public color: ThemePalette = 'primary';
classrooms: any;
exams: any;
offices: any;
notSelected = true;
filteredClassrooms: any;
isError = false;
errorMessage : any

constructor(
  public dialogRef: MatDialogRef < NewEventDialogComponent >,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  private eventService: EventService, private _snackBar: MatSnackBar) {
  this.classrooms = data.classrooms;
  this.exams = data.exams;
  this.offices = data.offices;
  this.filteredClassrooms = this.classrooms;
  this.minDate = moment()
}

  public form = new FormGroup({
  office: new FormControl(null, [Validators.required]),
  classroom: new FormControl(null, [Validators.required]),
  exam: new FormControl(null, [Validators.required]),
  start: new FormControl(null, [Validators.required]),
  end: new FormControl(null, [Validators.required]),
  peopleCount: new FormControl(null, [Validators.required])
})

ngOnInit(): void {
}

changeData(event: any){
  if (event != null) {
    this.notSelected = event != null ? false : true;
    this.filteredClassrooms = this.classrooms.filter(item => item.officeId == event)
  }
}
 async save() {
  let promise = await this.eventService.addEvent(this.form.value).catch(
    (error) => {
      this.isError = true;
      this.errorMessage = error.error.message;
    }
  )


  if(!this.isError){
    this._snackBar.open("Successfully added", "Close");
    this.dialogRef.close(this.form.value);
  }
}

close() {
  console.log(this.form.value);
  this.dialogRef.close();
}

}
