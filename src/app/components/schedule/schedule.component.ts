import { Component, OnChanges, OnInit, SimpleChanges, Input, ViewChild, ViewContainerRef, TemplateRef, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { CalendarApi, CalendarOptions, FullCalendarComponent } from '@fullcalendar/angular';
import { EventService } from 'src/app/services/event.service';
import { OfficeService } from 'src/app/services/office.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { CalendarEvent } from './events';
import ruLocale from '@fullcalendar/core/locales/ru'
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { NewEventDialogComponent } from '../new-event-dialog/new-event-dialog.component';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnChanges, OnInit, AfterViewInit {

  @Input() exams:any;
  @Input() classrooms:any;
  constructor(private tokenService: TokenStorageService, private router: Router, private eventService: EventService, private officeService: OfficeService) {
    if (!tokenService.isLoggedIn()) {
      router.navigate(['/signin'])
    }
    officeService.getOffices()
  }

  ngOnInit() {

  }

  function(){

  }
  ngAfterViewInit() {

  }

  ngOnChanges(changes: SimpleChanges) { }

  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    displayEventEnd: true,
    schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
    height: 'auto',
    themeSystem: 'bootstrap',
    nowIndicator: true,
    resourceGroupField: 'office',
    locale: ruLocale,
    resourceAreaWidth: '50%',

    headerToolbar: {
      left: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek,resourceTimelineFourDays',
      center: 'title'
    },
    buttonText: {
      today: 'Сегодня',
      month: 'Месяц',
      week: 'Неделя',
      day: 'День',
      list: 'Лист',
      resourceTimelineFourDays: "По офисам"
    },
    views: {
      resourceTimelineFourDays: {
        type: 'resourceTimeline',
        duration: { days: 4 }
      }
    },
    resources: [],
    events: [],
  }

}
