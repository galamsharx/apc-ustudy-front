import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Inject, Injectable, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { NewEventDialogComponent } from '../new-event-dialog/new-event-dialog.component';

@Injectable({
  providedIn: 'root',
})
export class ExamDialogData {
  exams: any;
  offices: any;
}

@Component({
  selector: 'app-region-settings',
  templateUrl: './region-settings.component.html',
  styleUrls: ['./region-settings.component.css']
})
export class RegionSettingsComponent implements OnInit {

  offices: any;
  exams: any;
  selectedOffice: any;
  selectable = false;
  removable = true;
  notSelected = true;
  filteredExams: any;
  isError = false;
  errorMessage : any;

  constructor(
    public dialogRef: MatDialogRef<NewEventDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ExamDialogData,
    private tokenService: TokenStorageService,
    private http: HttpClient
  ) {
    this.offices = data.offices;
    this.exams = data.exams;
  }

  public form = new FormGroup({
    officeId: new FormControl(null, [Validators.required]),
    examId: new FormControl(null, [Validators.required])
  })

  ngOnInit(): void {
  }

  changeData(event: any) {
    if (event != null) {
      this.notSelected = event != null ? false : true;
      let office = this.offices.filter(item => item.id == event)
      this.filteredExams = office[0].exams
      this.exams = this.exams.filter(item => !this.filteredExams.includes(item))
    }
  }

  add() {
    let headers = new HttpHeaders().set("Authorization", "Bearer " + this.tokenService.getToken())
    this.http.post("https://webapi20210729133449.azurewebsites.net/api/v1/offices/add/exam",this.form.value, {headers}).subscribe(
      result => {
          return result;
      },
      error => {
          this.isError = true;
          this.errorMessage = error.error.message;
      }
  )
  }

  save() {

  }

  close() {
    this.dialogRef.close();
  }
}
