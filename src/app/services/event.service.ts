﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { TokenStorageService } from './token-storage.service'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

const API_URL = 'https://localhost:5001/api/v1/proposals'

@Injectable({
    providedIn: 'root'
})

export class EventService {
    form: FormGroup;
    constructor(private http: HttpClient, private tokenService: TokenStorageService, private formBuilder: FormBuilder, private router: Router) {
        this.form = this.formBuilder.group({
            classroomId: this.formBuilder.control(null, Validators.required),
            actions: this.formBuilder.array(
                [
                    {
                        type: 0,
                        people: null,
                        examId: null,
                        date: null
                    },
                    {
                        type: 1,
                        people: null,
                        examId: null,
                        date: null
                    }]
            , Validators.required)
        })
    }

    async getEvents() {
        var headers = new HttpHeaders().set("Authorization", "Bearer " + this.tokenService.getToken())
        return await this.http.get(API_URL + "/region", { headers }).toPromise()

    }

    async addEvent(event) {
        var headers = new HttpHeaders().set("Authorization", "Bearer " + this.tokenService.getToken())
        this.form.value.classroomId = event.classroom;
        this.form.value.actions[0].people = event.peopleCount,
        this.form.value.actions[0].examId = event.exam,
        this.form.value.actions[0].date = event.start.toDate(),
        this.form.value.actions[1].people = event.peopleCount,
        this.form.value.actions[1].examId = event.exam,
        this.form.value.actions[1].date = event.end.toDate(),
        console.log(this.form.value)
        return this.http.post(API_URL,this.form.value, {headers}).toPromise()
    }

    async approve(proposalId){
        var headers = new HttpHeaders().set("Authorization", "Bearer " + this.tokenService.getToken())
        var params = new HttpParams().set("proposalId", proposalId)
        this.http.get(API_URL + "/approve",{params: params, headers: headers}).toPromise()
    }

    async reject(proposalId){
        var headers = new HttpHeaders().set("Authorization", "Bearer " + this.tokenService.getToken())
        var params = new HttpParams().set("proposalId", proposalId)
        this.http.get(API_URL + "/reject",{params: params, headers: headers}).toPromise()
    }
}