﻿import { Injectable } from '@angular/core';

const TOKEN_KEY = 'token';
const USER_KEY = 'auth-user';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {
  constructor() { }

  signOut(): void {
    localStorage.clear();
  }

  public saveToken(token: string): void {
    localStorage.removeItem(TOKEN_KEY);
    localStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string | null {
    return JSON.parse(localStorage.getItem(TOKEN_KEY)).accessToken;
  }

  public saveUser(user:any): void {
    localStorage.removeItem(USER_KEY);
    localStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  public getUser(): any {
    const user = localStorage.getItem(USER_KEY);
    if (user) {
      return JSON.parse(user).user;
    }

    return {};
  }

  public isLoggedIn(): boolean {
      var expires = JSON.parse(localStorage.getItem('expiration'));
      if(new Date(expires).getTime() < new Date().getTime()){
          this.signOut()
          return false;
      }
      return true
  }
}