﻿import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TokenStorageService } from './token-storage.service';

const API_URL = 'https://localhost:5001/api/v1/classrooms'

@Injectable({
    providedIn: 'root'
})

export class ClassroomService {
    constructor(private http: HttpClient, private tokenService: TokenStorageService) { }

    async getClassrooms(){
        var headers = new HttpHeaders().set("Authorization", "Bearer " + this.tokenService.getToken())
        return await this.http.get(API_URL,{headers}).toPromise()
    }
}